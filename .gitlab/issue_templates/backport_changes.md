## Why is this backport needed (background and context)

<!--
Use this template if you want to backport documentation changes to a stable branch.

Make sure to accurately describe why this backport is needed. Links to other
issues or customer tickets can help a lot.

Backporting documentation to older branches is something that should be used rarely.
The criteria includes legal issues, emergency security fixes, and fixes to content that
might prevent users from upgrading or cause data loss.

Maintainers (backend, frontend, docs) can backport changes, usually bug fixes but
also important documentation changes, into the latest stable version. To guarantee
the [maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html)
is respected, merging to older stable branches is restricted to release
managers.
-->

- Merge request to backport:

## Get the approval of TW Leadership

> The person requesting the backport does this step.

If the backport is for a version older than the latest stable branch, ask
Technical Writing Leadership for approval:

- [ ] Mention TW Leadership in a comment in this issue and wait for their approval:

  ```md
  @gitlab-org/tw-leadership could I get your approval for this documentation backport?

## Create the merge request to backport the change

> The person requesting the backport does this step. Requires at least the
> Developer role on the project that needs the backport.

To backport a change, you will merge your changes into the stable branch of the version
where you want the change to occur.

1. [ ] Open a merge request with the backport. The MR should target the stable release branch,
   for example: `16-11-stable-ee` or `17-0-stable-ee`. Link the MR to this issue.
1. [ ] If the backport targets the current stable branch, assign to a Technical Writer
   for review and merge. Usually, the Technical Writer of the [relevant group](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments).
1. [ ] If the backport targets any branch other than the latest stable branch,
   assign the MR to a Technical Writer for review. After the TW approves,
   ask a [release manager](https://about.gitlab.com/community/release-managers/)
   to review and merge the change. Mention this issue to them and give them all
   the context they need.

For the change to appear in docs.gitlab.com, merging to the stable branch is all you need.

For the change to appear under `/help`, the change needs to be part of a GitLab release.
The next time a release manager creates a release, they will include the change in the release.

## Deploy the backport change

> A member of the Technical Writing team does this step. Usually, the same Technical Writer
> who created the merge request.

After the change is backported to a stable branch, the Docker image that holds that version's
docs needs to be updated. These Docker images determine the contents that are displayed when
you select the **Version** drop-down list on the upper-right corner of the docs site.

1. [ ] Run a [new pipeline](https://gitlab.com/gitlab-org/gitlab-docs/-/pipelines/new)
   in `gitlab-docs`. Choose the branch name that matches the stable version, for example `15.11` or `16.0`.

If the backport change was made to one of the last three stable branches, you
need to update the main docs site:

1. [ ] After the pipeline finishes and the Docker image is updated, go to the
   [pipeline schedules](https://gitlab.com/gitlab-org/gitlab-docs/-/pipeline_schedules)
   and run the **Build docker images manually** schedule.
1. [ ] Open the **Build docker images** pipeline you just started, find the `image:docs-latest` job,
   and start it manually.
1. [ ] When the `image:docs-latest` job is done, run a
   [new pipeline](https://gitlab.com/gitlab-org/gitlab-docs/-/pipelines/new)
   that targets the `main` branch.
1. [ ] After the pipeline finishes, go to <https://docs.gitlab.com> and verify that the changes are available for the respective version.

If the backport change was made to a version other than the last three stable
branches, you need to update the docs archives site:

1. [ ] Run a [new pipeline](https://gitlab.com/gitlab-org/gitlab-docs-archives/-/pipelines/new)
   in `gitlab-docs-archives`.
1. [ ] After the pipeline finishes, go to <https://archives.docs.gitlab.com> and verify that the changes are available for the respective version.

/label ~"docs-backport" ~"Technical Writing"
