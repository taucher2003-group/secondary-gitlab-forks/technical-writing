## Engineer, Technical Writing onboarding

### Manager tasks

The manager for the person being onboarded must complete these tasks:

1. [ ] On the second day, a [role based entitlement Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/baseline-entitlements/#role-entitlements-for-a-specific-job) will be created automatically for the new team member and you'll be tagged in it. In the issue, make sure the team member is added to:
   - `docs@gitlab.com` and `ux-department@gitlab.com`.
   - `@docsteam` user group in Slack (they'll be added to `#docs` and `#tw-team`).
   - [`gl-docsteam` group](https://gitlab.com/groups/gl-docsteam/-/group_members) on GitLab.com.
1. [ ] Add the new team member as a Direct member of of the [`gitlab-ux` sub-group](https://gitlab.com/groups/gitlab-com/gitlab-ux/-/group_members?with_inherited_permissions=exclude).
1. [ ] Add the new team member to the [`ux-retrospectives` project members](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/project_members)
1. [ ] Add the new team member to the following files:
   - [`sites/handbook/blob/main/content/handbook/product/ux/technical-writing/_index.md`](https://gitlab.com/gitlab-com/content-sites/handbook/blob/main/content/handbook/product/ux/technical-writing/_index.md)
   - [`sites/handbook/source/includes/stages/tech-writing.html.haml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/includes/stages/tech-writing.html.haml)
1. [ ] Add the new team member to skip level meetings with the UX VP.

### Team member tasks

Welcome to the Technical Writing team! Here are your team-related onboarding tasks:

#### Week 1-2

1. [ ] Say "hi" to the rest of the Technical Writing team in the #tw-team
   channel in Slack. Tell us a bit more about yourself by using the
   [Team Agenda doc](https://docs.google.com/document/d/1XRyVjR5G21Amq4QqJs9jbV0BVQquiBAyPR257-hT1JY/edit),
   and then during the first team meeting you're able to attend.
1. [ ] Join the team's [Slack channels](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#contact-us),
   and #docs-team-onboarding. In the latter channel, ask any questions that
   arise during your onboarding. Don't worry about asking seemingly basic
   questions; everyone has joined the team bringing their own knowledge and
   experience, and some of our practices may not be obvious or documented.
   You can improve our onboarding (or other process documentation) by creating
   MRs, issues, or mentioning ideas in the Slack channel.
1. [ ] Join the #product Slack channel.
1. [ ] In Slack, search for "ux" and join those UX channels that seem relevant, including #ux-private and location-specific channels.
1. [ ] Read the following to become familiar with how technical writing is done
   at GitLab:
   - [ ] [Technical Writing Handbook](https://handbook.gitlab.com/handbook/product/ux/technical-writing/).
   - [ ] Successfully complete the [GitLab Technical Writing Fundamentals](https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals/technical-writing-fundamentals/) course.
   - [ ] [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation)
     and the various pages linked from the introduction. Note that there are
     [Company writing style guidelines](https://handbook.gitlab.com/handbook/communication/#writing-style-guidelines)
     in addition to our [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
      - [ ] Per <https://docs.gitlab.com/ee/development/documentation/site_architecture/#source-files>,
        begin to familiarize yourself with the `gitlab` project's `doc`
        directory.
      - [ ] In addition to the projects where content is maintained (like
        `gitlab` and `runner`), review these key projects the team uses:
        - <https://gitlab.com/gitlab-org/technical-writing>: Process issues.
        - <https://gitlab.com/gitlab-org/gitlab-docs>: Documentation site
          issues (including the frontend, backend, and CI).
   - [ ] Search [Google Drive](https://drive.google.com) for the latest "State of the Docs" presentation, and review the slides.
   - [ ] Review these processes, noting the mentions of "documentation" and
     "technical writer"/"technical writing":
     - [GitLab Release Posts](https://handbook.gitlab.com/handbook/marketing/blog/release-posts/#tw-lead)
     - [Deprecating GitLab features](https://docs.gitlab.com/ee/development/deprecation_guidelines/index.html)
     - [UX design](https://handbook.gitlab.com/handbook/product/ux/product-designer/#partnering-with-technical-writers)
     - [Product development](https://handbook.gitlab.com/handbook/product-development-flow/)
     - [Product operations](https://handbook.gitlab.com/handbook/product/product-operations/)

     For the Release Post items, reviews of each feature's details are reviewed by
     the technical writer assigned to each stage/group. This is specified only in the [MR template](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/merge_request_templates/Release-Post-Item.md), and
     not in the Handbook page.
1. [ ] Create your own test project in <https://gitlab.com/gitlab-org/technical-writing-group/>.
   You can use this project as a sandbox to test most GitLab features safely, at
   your own pace. It's also a good way to practice Git commands locally without working
   in a "production" project. A test project in this group is better than under
   your personal namespace because it has full access to all GitLab features, up
   to the Ultimate tier.
   1. Go to the [Technical Writing Team's group](https://gitlab.com/gitlab-org/technical-writing-group/)
      and select **Create project > Create blank project**.
   1. Set the project name as `Your name test project`.
   1. Set the **Visibility level** to public.
   1. Make sure **Initialize repository with a README** is selected, and **Enable Static Application Security Testing (SAST)**
      is *not* selected.
   1. Select **Create project**.
   1. When finished, ping your manager and ask them to [add you to the project](https://docs.gitlab.com/ee/user/project/members/#add-users-to-a-project)
      with the maintainer role.
1. [ ] [Set up your computer](https://handbook.gitlab.com/handbook/product/ux/technical-writing/setup/)
   for writing and previewing GitLab product documentation.
1. [ ] Familiarize yourself with GitLab's [merge requests guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).
1. [ ] The Technical Writing team uses the Geekbot app in Slack to run
   asynchronous standups, which include the 'Morning Stretch' and
   'Wednesday Weekly' questions. Ask your onboarding buddy who can add you to
   the application.
1. [ ] If you want to start feeling productive beyond your training, ask your
   onboarding buddy for a simple task to get you started.
1. [ ] Verify you have access to view the UX Meetings calendar, and add the calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_9psh26fha3e4mvhlrefusb619k@group.calendar.google.com` and then press Enter on your keyboard. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar.
1. [ ] Subscribe to the Technical Writing Team calendar. The calendar ID is `gitlab.com_vht0s4pdfh8m1p9a4gs38b9ru8@group.calendar.google.com`. The team uses this calendar to share OOO events. Then, [sync the calendar with PTO Deel](https://handbook.gitlab.com/handbook/people-group/engineering/team-pto-calendar/#steps) so your OOO events are also added to the calendar.
1. [ ] As you're reading, see if you can find the answers to
   [these questions](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/tw_quiz.md). If you're unsure of
   something, ask your buddy or post in Slack. When you're ready, you can
   [check your answers](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/answer_key.md).
1. [ ] Submit a request for [Light Agent access to ZenDesk](https://handbook.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account) so that you can view support tickets.
1. [ ] To get access to our UX design tool, log in to [Figma](https://www.figma.com) with your GitLab Google account.
1. [ ] Make sure your work email in Workday and the [email you use for notifications in your GitLab profile](https://gitlab.com/-/profile/notifications) are the same. Our performance indicators rely on this information.

1. [ ] Submit an access request to become a maintainer for these projects:
   - [`gitlab-docs`](https://gitlab.com/gitlab-org/gitlab-docs)
   - [`gitlab-docs-hugo`](https://gitlab.com/gitlab-org/technical-writing-group/gitlab-docs-hugo)
   - [`gitlab-docs-archives`](https://gitlab.com/gitlab-org/gitlab-docs-archives)
   - [`technical-writing`](https://gitlab.com/gitlab-org/technical-writing)

   You do not need maintainer permissions for the [`handbook`](https://gitlab.com/gitlab-com/content-sites/handbook) project. Anyone with developer permissions can merge MRs, except if the MR is for a controlled document that requires CODEOWNER approval.

   Use the [`Individual_Bulk_Access_Request` template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request)
   and tag your manager.
1. [ ] When you have maintainer access to the previously listed projects, submit an MR
   to update your [team member file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person)
   with your project permissions. Change `reviewer` to `maintainer` for all
   the projects you have maintainer access to.
1. [ ] Submit an MR to add yourself as a `docs.gitlab.com` administrator to the Google Search Console.
   Follow the instructions to add an [HTML `meta` tag](https://support.google.com/webmasters/answer/9008080?hl=en&ref_topic=9455938#zippy=%2Chtml-tag):

   1. Go to <https://search.google.com/search-console>.
   1. In **URL Prefix**, enter `https://docs.gitlab.com` and select **Continue**.
   1. Expand the **HTML Tag** section, and copy the code given.
   1. Open an MR to add the tag and your name to the list at <https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/layouts/head.html#L64>.
   1. Submit the MR to your manager for approval and merge.

1. [ ] Read the following to become familiar with how engineering work is done
   at GitLab:
      - [ ] [Frontend development guidelines](https://docs.gitlab.com/ee/development/fe_guide/)
      - [ ] [Go guide](https://docs.gitlab.com/ee/development/go_guide/)
      - [ ] [Code review best practices](https://docs.gitlab.com/ee/development/code_review.html#best-practices)
      - [ ] [Frontend testing](https://docs.gitlab.com/ee/development/testing_guide/frontend_testing.html)
      - [ ] [Tailwind CSS](https://docs.gitlab.com/ee/development/fe_guide/style/scss.html#tailwind-css)
      - [ ] [Frontend style guides](https://docs.gitlab.com/ee/development/fe_guide/style/)

1. [ ] Familiarize yourself with the Docs tech stack. Feel free to skip trainings on subjects you're already familiar with, or just bookmark references for later.
      - [ ] Hugo
         - [ ] [Hugo documentation](https://gohugo.io/documentation/)
         - [ ] [Video tutorials](https://gohugo.io/getting-started/external-learning-resources/#hugo-beginner-tutorial-series)
         - [ ] [Hugo in Action](https://learning.oreilly.com/library/view/hugo-in-action/9781617297007/). You can access O'Reilly books via Okta.
      - [ ] [Vue.js](https://vuejs.org/guide/introduction.html)
      - [ ] [Go](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/golang_training.md)
      - [ ] [GitLab CI/CD](https://university.gitlab.com/learning-paths/certified-ci-cd-specialist-learning-path?sessionFields=%5B%5B%22subject%22%2C%22CI%2FCD%22%5D%5D)
      - [ ] [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui) and [Pajamas Design System](https://design.gitlab.com/)

1. [ ] Within your first two months, complete a [GitLab Secure Development Short Course](https://portal.securecodewarrior.com/#/courses/course-list/e6a9e788-8aa7-4aeb-9849-9072c00e6f58) in Vue.js, Node.js, or Go. You should gain access to Secure Code Warrior via a tile in Okta during your first few days. If not, request access from the Application Security team by posting a comment in #sec-appsec.

1. [ ] Optionally, join any engineering-related Slack channels of interest. Some examples: `#accessibility` `#dev_tip_of_the_day` `#development` `#f_markdown` `#frontend` `#frontend_testing` `#g_pajamas-design-system` `#gitlab-pages` `#gitlab-ui` `#golang` `#hugo` `#infra-read-feed` `#wg_vue_migration`

1. [ ] Add the links to other issues to which you're assigned or MRs you
   authored or reviewed that don't pertain to the group, if any:

/label ~"onboarding" ~"Technical Writing"
