# Technical Writing team onboarding

## Manager tasks

1. [ ] On the second day, you should be mentioned in [an issue](https://internal.gitlab.com/handbook/it/end-user-services/access-request/baseline-entitlements/#role-entitlements-for-a-specific-job) that's assigned to the writer. Verify that the issue includes access to:
     - [ ] Email groups: `docs@gitlab.com`, `ux-department@gitlab.com`, `product@gitlab.com`
     - [ ] Slack group: `@docsteam` (includes the `#docs` and `#tw-team` channels)
     - [ ] GitLab group: [`gl-docsteam`](https://gitlab.com/groups/gl-docsteam/-/group_members)
1. Determine and assign:
   - [ ] Secondary group for shadowing:
   - [ ] Primary group for trainee work:
   - [ ] Schedule for future group assignments:
1. Add the team member to:
   - [ ] [Product Division Monthly](https://docs.google.com/document/d/127ynPa3gPtnI0K3hsdTuDk8Or2AOvnzn16lbFY5BHvw/edit#heading=h.f7bxfblpdayt) meeting
   - [ ] [TW milestone plan](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-milestone-plan.md) as assignee
   - [ ] [`gitlab-ux` subgroup](https://gitlab.com/groups/gitlab-com/gitlab-ux/-/group_members?with_inherited_permissions=exclude) as a direct member
   - [ ] [`ux-retrospectives` project](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/project_members) as a direct member
   - [ ] Any private Slack channels
     - [ ] `#company-fyi-private`
     - [ ] `#product-private`
     - [ ] `#ux-private`
   - [ ] Skip-level meetings with Technical Writing Director
   - [ ] Geekbot

## Team member tasks

Welcome to the Technical Writing team! Please complete these tasks over the following weeks.

### Weeks 1-2 - Getting started

#### Communication and access

1. [ ] Meet the team.
   - Say "hi" in the `#tw-team` Slack channel.
   - Learn about the team by viewing people's README files (at `gitlab.com/username`). (Many people don't have one.)
   - Introduce yourself by answering the following questions in the [Team Agenda doc](https://docs.google.com/document/d/1XRyVjR5G21Amq4QqJs9jbV0BVQquiBAyPR257-hT1JY/edit). The team meeting time varies each week to cater to different time zones. You can read your answers and introduce yourself in the next meeting you can attend. If you want some inspiration, search the agenda to see how others answered when they joined.
     - Where are you located?
     - Where were you before GitLab?
     - Why did you want to join the team?
     - What do you like to do for fun outside of work?
1. [ ] Join Slack channels.
   - `#product` for general knowledge.
   - `#docs-team-onboarding` for asking onboarding questions. In this channel, you can ask anything you're curious about. Don't worry about asking seemingly basic questions; everyone was new at one point and some practices may not be obvious or documented. You can improve our onboarding (or other process documentation) by creating MRs, issues, or mentioning ideas in the Slack channel.
   - Any channels prefixed with `ux-` that seem relevant.
   - Any location-specific channels that interest you. (They're prefixed with `loc-`).
   - See the [team channel list](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#contact-us) for more.
1. Set up team calendars. Please don't remove meetings from shared or team calendars, as it removes the event for everyone.
   - [ ] [Subscribe](https://support.google.com/calendar/answer/37100#) to the `UX Meetings` calendar: `gitlab.com_9ps##6fha3e4mvhlrefusb619k@group.calendar.google.com`.
   - [ ] [Subscribe](https://support.google.com/calendar/answer/37100#) to the `Technical Writing Team` calendar: `gitlab.com_vht0s4pdfh8m1p9a4gs38b9ru8@group.calendar.google.com`.
   - [ ] [Sync with PTO Deel](https://handbook.gitlab.com/handbook/people-group/engineering/team-pto-calendar/#steps).
1. Set up accounts and access.
   - [ ] Request a [ZenDesk Light Agent account](https://handbook.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account) so that you can view support tickets.
   - [ ] Sign in to [Figma](https://www.figma.com), our UX design tool, with your GitLab Google account.
   - [ ] Verify your [GitLab profile notification email](https://gitlab.com/-/profile/notifications) matches Workday. Our performance indicators rely on this information.
   - [ ] Submit an [access request](https://handbook.gitlab.com/handbook/business-technology/end-user-services/onboarding-access-requests/access-requests/) for Anthropic Claude, the AI tool we use to assist us with work. Assign to your manager for approval.
   - [ ] Verify your manager added you to the [Geekbot](https://app.geekbot.com/dashboard/home) Slack app for standups.

#### Required reading

1. [ ] Review Tech Writing resources.
   - [ ] [Technical Writing Handbook](https://handbook.gitlab.com/handbook/product/ux/technical-writing/)
   - [ ] [Docs style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/)
   - [ ] [Docs word list](https://docs.gitlab.com/ee/development/documentation/styleguide/word_list.html)
   - [ ] [Company writing style guidelines](https://handbook.gitlab.com/handbook/communication/#writing-style-guidelines)
1. [ ] Complete the [GitLab Technical Writing Fundamentals course](https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals/technical-writing-fundamentals/).
1. [ ] Review doc architecture and projects.
   - [ ] [Documentation site architecture](https://docs.gitlab.com/ee/development/documentation/site_architecture/)
   - [ ] Primary projects:
     - [ ] [gitlab](https://gitlab.com/gitlab-org/gitlab/): Main project for GitLab. Most of the documentation is in the `/doc` folder.
     - [ ] [gitlab-docs](https://gitlab.com/gitlab-org/gitlab-docs): Project used to build the documentation site. The left navigation is also stored here.
     - [ ] [technical-writing](https://gitlab.com/gitlab-org/technical-writing): Team processes. Monthly milestone plans and OKR issues are here.
1. [ ] Review content processes. These processes require technical writers to complete specific tasks, like running scripts or validating formatting.
   - [ ] [GitLab Release Posts](https://handbook.gitlab.com/handbook/marketing/blog/release-posts/#tw-lead)
   - [ ] [Release Posts review checklist](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/merge_request_templates/Release-Post-Item.md)
   - [ ] [Deprecating GitLab features](https://docs.gitlab.com/ee/development/deprecation_guidelines/index.html)
1. [ ] Learn how we work with other teams.
   - [ ] [UX design](https://handbook.gitlab.com/handbook/product/ux/product-designer/#partnering-with-technical-writers)
   - [ ] [Product development](https://handbook.gitlab.com/handbook/product-development-flow/)

#### Technical setup

1. [ ] Set up your work environment.
   - [ ] [Configure your computer](https://handbook.gitlab.com/handbook/product/ux/technical-writing/setup/) for documentation work.
   - [ ] Review the [merge requests guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).
1. [ ] Create a sandbox project in the Technical Writing group. You can use this to test most GitLab features safely at your own pace.
   1. Go to [Technical Writing Team's group](https://gitlab.com/gitlab-org/technical-writing-group/).
   1. Select **Create project > Create blank project**.
   1. Configure the project.
      - Name: `<Your name> test project`
      - Visibility: Public
      - Select **Initialize repository with a README**
      - Clear **Enable Static Application Security Testing (SAST)**
   1. Ask your manager to assign you the Maintainer role for the project.

#### Assessment and practice

1. [ ] Complete the [technical writing quiz](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/tw_quiz.md).
   - If needed, ask questions in Slack.
   - [Check your answers](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/onboarding/answer_key.md).
1. [ ] Start contributing to the documentation.
   - Ask your onboarding buddy for a simple starter task.
   - Document any issues or merge requests you create in the following area.

TIP:
When you create issues or merge requests, add the links to this issue immediately. Don't wait until the end of the milestone.

#### Document your work - Getting started

Track your contributions here:

**MRs**:

**Issues**:

**Other notable work**:

### Release 1 - Shadow phase

During your first release, you'll shadow other technical writers as they work on documentation issues and MRs. You'll:

- Review issues and merge requests shared by the other writers.
- Participate in group communications and meetings.
- Ask questions to understand the documentation workflow.
- Learn about different documentation approaches across groups.

#### Group Assignments

1. [ ] Shadow other writers.
   - Group 1:
   - Group 2:
   - Group 3:
1. [ ] Research potential group assignments.
   - [ ] Review the GitLab [stages and groups](https://handbook.gitlab.com/handbook/product/categories/).
   - [ ] Discuss coverage needs with your manager.
   - Consider your interests and experience.
1. Document your group preferences.
   - [ ] Primary group:
   - [ ] Secondary group:
   - [ ] Alternative group:

#### Document your work - Shadow

Track your contributions here:

**MRs**:

**Issues**:

**Other notable work**:

### Release 2 - Trainee phase

During your second release cycle, you'll act as a trainee for two to three groups assigned by your manager. As a trainee, you'll work with the currently assigned writer to review documentation MRs. When you feel ready, you'll start to review roughly half of each group's MRs. This may include a variety of different work including documentation changes, UI text, feature deprecations, and release post content. You can also start making more substantial changes to the documentation now as well.

NOTE:
Your manager may extend the shadowing phase before you begin this trainee phase.

#### Learn about your groups

1. [ ] List your assigned groups.
   - Group 1:
   - Group 2:
   - Group 3:
1. [ ] Review the documentation and features for each group.
   - Review their vision pages
   - Read their documentation
   - Test features as possible
   - Review the docs-only issue board
1. [ ] Meet with Product Managers (PMs):
   - Discuss priorities and upcoming documentation needs
   - Establish communication preferences
   - Determine how to manage docs-only issues
1. [ ] Review milestone planning:
   - [ ] Review the [Technical Writing planning workflow](https://handbook.gitlab.com/handbook/product/ux/technical-writing/workflow/#planning).
   - [ ] Review the Technical Writing milestone plan for your groups. Each month you'll be asked to update a milestone plan [like this one](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-milestone-plan.md). You can view previous milestone plans in this issue and use them to more easily find upcoming planning issues.
   - [ ] Update board and planning issue links with the current Technical Writer.

#### Documentation changes

1. [ ] Review documentation MRs from others.
    - [ ] Notify the current writer that you're ready to begin reviewing MRs. They'll begin tagging you in MRs.
    - [ ] Review MRs that are assigned to you.
    - [ ] Add the MR to the following section in this issue.
    - [ ] When you complete your review, request a review from the current writer.
1. [ ] Author documentation MRs.
   - [ ] Read about [Documentation review apps](https://docs.gitlab.com/ee/development/documentation/review_apps.html).
   - [ ] Submit MRs for improvements and list them in the `Document your work` sections. These can be simple or "obvious" improvements, or larger if you like.
   - [ ] Create issues for future work.
   - [ ] Use a review app for at least one MR.
   - Apply appropriate labels for the stage, group, `documentation`, and `Technical Writing`.
1. [ ] Lead a small, but substantial, documentation project.
    - [ ] Discuss the project with your manager and the current writer.
    - [ ] Add a link to the planning issue:

#### Prepare for reviews

1. Modify your [team member file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) to add yourself as a reviewer.

   ```yaml
   # Add this after the departments section:
   projects:
     gitlab: reviewer docs
     omnibus-gitlab: reviewer docs
     gitlab-runner: reviewer docs
     gitlab-chart: reviewer docs
     gitlab-operator: reviewer docs
     gitlab-development-kit: reviewer docs
     gitlab-docs: reviewer
   ```

1. [ ] Learn about the [GitLab Review Workload Dashboard](https://gitlab-org.gitlab.io/gitlab-roulette/) (Review Roulette).
    - You might begin receiving MRs to review. Depending on your comfort level, either complete the review or pass it to another writer.
1. [ ] Learn about [Conventional Comments](https://conventionalcomments.org/) for better review comments.
   - [ ] Consider installing the [Chrome extension](https://gitlab.com/conventionalcomments/conventional-comments-button).

TIP:
If you're not ready to receive review requests or need a break after receiving too many, [set your GitLab status](https://docs.gitlab.com/ee/user/profile/#set-your-current-status) to something like: `bulb` (:bulb:) Onboarding, not currently accepting reviews.

#### Document your work - Trainee

Track your contributions here:

**MRs**:

**Issues**:

**Other notable work**:

### Release 3 - Primary writer phase

During your third release cycle, you'll act as the primary writer for the groups you worked with during the previous release cycle. As the primary writer, you'll review and author documentation changes. The previously assigned technical writer will serve as your coach. When you finish reviewing an MR, forward it to your coach for final review and merging. Coaches may share the burden of these reviews with other technical writers.

NOTE:
Your manager may extend the trainee phase before you begin the primary writer phase.

#### Submit Access Requests

1. [ ] Add yourself as a `docs.gitlab.com` administrator for Google Search Console Access.
   1. Go to the [Google Search Console](https://search.google.com/search-console).
   1. Enter `https://docs.gitlab.com` in **URL Prefix** and select **Continue**.
   1. Expand **HTML Tag** section and copy the code.
   1. Create an MR to add the tag and your name to [head.html](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/layouts/head.html#L64).
   1. Assign the MR to your manager for approval.
1. [ ] Create an [Individual Bulk Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request the Maintainer role for GitLab projects and groups. This role gives you the ability to merge MRs. Assign the MR to your manager for approval.
   - [`gitlab`](https://gitlab.com/gitlab-org/gitlab)
   - [`gitlab-docs`](https://gitlab.com/gitlab-org/gitlab-docs)
   - [`technical-writing`](https://gitlab.com/gitlab-org/technical-writing)
   - [`www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com)
   - [`omnibus-gitlab`](https://gitlab.com/gitlab-org/omnibus-gitlab)
   - [`charts/gitlab`](https://gitlab.com/gitlab-org/charts/gitlab)
   - [`cloud-native/gitlab-operator`](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator)
   - [`gitlab-development-kit`](https://gitlab.com/gitlab-org/gitlab-development-kit)
   - [`runner-docs-maintainers`](https://gitlab.com/groups/gitlab-com/runner-docs-maintainers/-/group_members?sort=access_level_desc)
   - [`runner-maintainers`](https://gitlab.com/groups/gitlab-com/runner-maintainers/-/group_members?sort=access_level_desc)
   - [`gitlab_kramdown`](https://gitlab.com/gitlab-org/gitlab_kramdown) (Only one Technical Writer per region needs this)
1. [ ] When completed, update your [team member file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person):
   - Change `reviewer` to `maintainer` for all projects where you now have the Maintainer role.

#### Document your work - Primary writer

Track your contributions here:

**MRs**:

**Issues**:

**Other notable work**:

/label ~"onboarding" ~"Technical Writing"
