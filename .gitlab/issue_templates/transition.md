<!-- The issue name should be in the form: Transition [stage]/[group] TW assignment to [new writer] -->
<!-- Create separate issues for each group being transitioned to a different technical writer. -->

- Stage/group being transitioned:
- Current technical writer:
- Incoming technical writer:
- Reassignment milestone:

## Tasks for current technical writer

- [ ] Invite the incoming TW to the stage and group Slack channels.
  - Channel list: _TBD_
- [ ] Post in Slack about the changeover. Communicate the timeline and how MRs will be handled.
- [ ] Reach out to the Product Manager, Engineering Manager, and Design Manager to communicate the change.
  - [ ] PM:  _TBD_
  - [ ] EM:  _TBD_
  - [ ] PDM: _TBD_

### Provide group insights

To make the transition easier for the incoming technical writer, answer these questions in a comment thread. Consider making the comment an **internal note**.
Give as much detail as you feel is needed. The incoming technical writer should feel free to ask questions about any of
these details:

- [ ] How does the Product Manager plan work for future milestones? Provide links to relevant boards.
- [ ] How does the group prefer to communicate in issues and MRs regarding reviews?
- [ ] What is the [maturity](https://about.gitlab.com/direction/maturity/) of the feature set and documentation for
      the feature set?
- [ ] How well are features of the group known to people outside the group?
- [ ] How much ~"UI text" work does the group generate?
- [ ] Can the feature set for the group be explored solely on GitLab.com, or does it also require GDK or access to
      third-party tools?
- [ ] Does the group attract a lot of ~"Support Team Contributions" and ~"Community contribution"?

## Tasks for EMs and PMs

- [ ] Add the incoming TW to team meetings and any group aliases.
  - Meeting list: _TBD_
  - Group aliases: _TBD_
- [ ] Async retrospectives (if applicable):
  - Update [teams.yml](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml).
  - If the retrospective happens in a private project, add the TW to it.
- [ ] Remove the outgoing TW from team meetings and any group aliases.
- [ ] Determine the resources needed by the incoming TW to learn the stage / group
  features, similar to an engineer's onboarding.

## Tasks for incoming technical writer

- [ ] Set up coffee chats with the group's:
  - [ ] Product manager
  - [ ] Engineering manager
  - [ ] [Frontend and backend team members](https://handbook.gitlab.com/handbook/product/categories/) (not necessary to meet all)
  - [ ] UX designer
- [ ] Read the product documentation associated with your group (as identified by the group attribute in documentation pages' metadata).
- In the `www-gitlab-com` repository, update:
  - [ ] [`data/stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml), with your new assignment. If you're onboarding as a new Technical Writer, you can wait until you've completed a full release cycle as a trainee before doing this step and the next steps.
- In the `gitlab` repository, update as needed (in the following order, but can be done in the same MR):
  1. [ ] The metadata in all related markdown files (if the group name changed).
  1. [ ] Group assignments in [`lib/tasks/gitlab/tw/codeowners.rake`](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/tasks/gitlab/tw/codeowners.rake).
  1. [ ] [`.gitlab/CODEOWNERS`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/CODEOWNERS) by running the [`tw:codeowners` Rake task](https://docs.gitlab.com/ee/development/documentation/metadata.html#batch-updates-for-tw-metadata) to ensure no pages are missed.
- [ ] If the group was previously assigned to the `#docs` channel, add it to the [TW milestone plan template](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-milestone-plan.md).

## Tasks for technical writing manager

- [ ] Update google sheets used for team rebalancing planning.
- [ ] Check Workday for the Job Title Specialty (Multi-Select). If the specialty does not exist, create an issue in the People Group [general project](https://gitlab.com/gitlab-com/people-group/people-tools-technology/general), and use the **Workday: Job Title Specialty Request** template.
- [ ] Update Workday with the new group assignments. Log a [HelpLab Job Title Specialty Change request](https://helplab.gitlab.systems/esc?id=emp_taxonomy_topic&topic_id=6de369b997804e50a326158de053af7b) for the People group to update Workday with the new group assignments. Workday updates the group assignments in `data/team_members/person`.

/label ~"Technical Writing"
