## Issue description

<!-- Title this issue: `FY2_Q_ Stage Lead planning issue: STAGE` -->

This issue outlines the themes that will be the focus of the `STAGE` stage lead during `Q_` of `FY2_`.

### Themes

<!-- Themes are a way of grouping improvements together, working across the stage where possible. Examples of themes: technical accuracy, tutorials, use case-based workflows, getting started, identifying and filling gaps, etc.  -->

#### `Example theme: Identify and fill gaps in feature doc coverage`

- Short description: <!-- Example text: Expand documentation coverage. -->
- Reasoning: <!-- Example text: Several common SAST features are not documented. These changes will provide users with the information they need to successfully use these features. -->
- Note: <!-- Example text: These issues were carried over from FY25Q3.  -->

| Issue | Group(s) | Status | Effort | Priority | Details |
|-------|----------|--------|--------|----------|---------|
| `issue link with + to see description` | `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |
| `issue link with + to see description` |  `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |

#### `Example theme: Tutorials`

- Short description: <!-- Example text: Create tutorials that match or complement feature demonstrations recorded by SD engineers. -->
- Reasoning: <!-- Several important features have been added recently to SD. Providing matching tutorials helps users become familiar with the features. -->
- Note: <!-- Example text: Consider adding links to new tutorial content to the top-level tutorial pages at https://docs.gitlab.com/ee/tutorials/.  -->

| Issue | Group(s) | Status | Effort | Priority | Details |
|-------|----------|--------|--------|----------|---------|
| `issue link with + to see description` |  `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |
| `issue link with + to see description` |  `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |

#### `Example theme: Improve troubleshooting content`

- Short description: <!-- Example text: Improve security policy troubleshooting content -->
- Reasoning: <!-- Latest research verbatims indicated that users find it difficult to find relevant troubleshooting content. -->

| Issue | Group(s) | Status | Effort | Priority | Details |
|-------|----------|--------|--------|----------|---------|
| `issue link with + to see description` |  `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |
| `issue link with + to see description` |  `group` | Waiting for `<...>` | `Low, Medium, High` | `Low, Medium, High` | Target milestone is `17.x`. Led by: `Technical Writer (TW name)` or `Engineer` |

Legend:

- :hourglass_flowing_sand: : Waiting for `<...>`. `technical writer`, or `PM input`, or `Engineering input`
- :construction: : In progress
- :white_check_mark: : Complete
- :lifter_tone3: : Stretch goal (add next to any item that is aspirational this quarter)

### Ongoing tasks

Manage TW-DRI assignments for all milestones:

<!--
- [Create issue board](https://gitlab.com/groups/gitlab-org/-/boards/2159734?label_name[]=devops%3A%3Acreate)
- [Plan issue board](https://gitlab.com/groups/gitlab-org/-/boards/7825333)
- [Secure issue board](https://gitlab.com/gitlab-org/gitlab/-/boards/7602348?label_name[]=devops%3A%3Asecure)
- [Verify issue board](https://gitlab.com/groups/gitlab-org/-/boards/7318948?label_name[]=devops%3A%3Averify&label_name[]=documentation)
 -->

Specific [TW milestone planning issues](https://gitlab.com/gitlab-org/technical-writing/-/issues/?sort=created_date&state=opened&search=milestone%20plan&in=TITLE):

- [ ] [X.Y](issue link). Release date: YYYY-MM-DD
- [ ] X.Y. Release date: YYYY-MM-DD
- [ ] X.Y. Release date: YYYY-MM-DD

### References

- [Stage leads | TW Handbook](https://handbook.gitlab.com/handbook/product/ux/technical-writing/workflow/#stage-leads)
- Previous issue: `issue link`
