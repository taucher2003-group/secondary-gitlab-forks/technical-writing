import fs from "fs";
import yaml from "js-yaml";

export const CONSTANTS = {
  DOCS_BASE_URL: "https://docs.gitlab.com",
  DOCS_DIR: "../gitlab-docs",
};

/**
 * Get data sources consumed by Nanoc.
 *
 * @returns Array
 */
export const nanocDataSources = () => {
  const nanocConfig = yaml.load(
    fs.readFileSync(`${CONSTANTS.DOCS_DIR}/nanoc.yaml`, "utf8")
  );
  return nanocConfig.data_sources.filter((source) => source.items_root !== "/");
};

/**
 * Retrieve the global docs site navigation.
 *
 * @returns Object
 */
export const getNav = () => {
  return yaml.load(
    fs.readFileSync(
      `${CONSTANTS.DOCS_DIR}/content/_data/navigation.yaml`,
      "utf8"
    )
  );
};

/**
 * Find the title from a given markdown file.
 *
 * @returns String
 */
export const getTitleFromMarkdownPage = (fileContent) => {
  const lines = fileContent.split(/\r?\n/);
  let title = "";
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i].trim();
    if (line.startsWith("# ")) {
      title = line.substring(2);
      break;
    }
  }
  return title;
};

export const getHTMLpathFromMdPath = (source, filename) => {
  return (
    source.items_root.replaceAll("/", "") +
    filename
      .replace(source.content_dir, "")
      .replace(source, "")
      .replace("index.md", "")
      .replace(".md", ".html")
  );
};
